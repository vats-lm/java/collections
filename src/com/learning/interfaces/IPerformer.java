package com.learning.interfaces;

public interface IPerformer {

    void action();
}
