package com.learning.interfaces;

import com.learning.pojos.SimplePojo;

import java.util.AbstractList;

public interface IListOperator {

    void operate(AbstractList<SimplePojo> list, int times);
}
