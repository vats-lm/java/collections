package com.learning.main;

import com.learning.interfaces.IListOperator;
import com.learning.pojos.SimplePojo;
import com.learning.util.AddOperator;
import com.learning.util.GetOperator;
import com.learning.util.PerformanceTester;
import com.learning.util.RemoveOperator;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class ListChild {

    static AbstractList<SimplePojo> list;

    public static void main(String[] args) {

        int times = 2;

        System.out.println("--------------------------------------------------");
        System.out.println("Testing ADD operations");
        System.out.println("--------------------------------------------------");
        testAndPrintResult(new AddOperator(), times);

        System.out.println("--------------------------------------------------");
        System.out.println("Testing REMOVE operations");
        System.out.println("--------------------------------------------------");
        testAndPrintResult(new RemoveOperator(), times);

        System.out.println("--------------------------------------------------");
        System.out.println("Testing GET operations");
        System.out.println("--------------------------------------------------");
        testAndPrintResult(new GetOperator(), times);
    }

    private static void testAndPrintResult(IListOperator operator, int times) {

        long alTime = PerformanceTester.printExecutionTime("ArrayList -", () -> {
            list = new ArrayList<>();
            operator.operate(list, times);
        });

        long llTime = PerformanceTester.printExecutionTime("LinkedList -", () -> {
            list = new LinkedList<>();
            operator.operate(list, times);
        });

        long vTime = PerformanceTester.printExecutionTime("Vector -", () -> {
            list = new Vector<>();
            operator.operate(list, times);
        });

        System.out.println("--------------------------------------------------");
        if (alTime > llTime)
            if (llTime > vTime)
                System.out.println("ArrayList > LinkedList > Vector");
            else if (alTime > vTime)
                System.out.println("ArrayList > Vector > LinkedList");
            else
                System.out.println("Vector > ArrayList > LinkedList");
        else if (llTime > vTime)
            if (alTime > vTime)
                System.out.println("LinkedList > ArrayList > Vector");
            else
                System.out.println("LinkedList > Vector > ArrayList");
        else if (alTime > llTime)
            System.out.println("Vector > ArrayList > LinkedList");
        else
            System.out.println("Vector > LinkedList > ArrayList");
        System.out.println("--------------------------------------------------");
    }
}
