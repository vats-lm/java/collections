package com.learning.util;

import com.learning.interfaces.IListOperator;
import com.learning.pojos.SimplePojo;

import java.util.AbstractList;

public class AddOperator implements IListOperator {

    @Override
    public void operate(AbstractList<SimplePojo> list, int times) {

        for (int i = 0; i < times; i++) {

            DataProvider.fillList(list);
        }
    }
}
