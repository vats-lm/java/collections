package com.learning.util;

import com.learning.pojos.SimplePojo;

import java.util.AbstractList;
import java.util.Collection;

public interface DataProvider {

    static void fillList(Collection<SimplePojo> list) {

        list.add(new SimplePojo("A1", 1));
        list.add(new SimplePojo("A12", 12));
        list.add(null);
        list.add(new SimplePojo("A1", 1));
        list.add(new SimplePojo("A3", 13));
        list.add(null);
        list.add(new SimplePojo("A10", 10));
    }
}
