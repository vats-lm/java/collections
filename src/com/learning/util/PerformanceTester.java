package com.learning.util;

import com.learning.interfaces.IPerformer;

public interface PerformanceTester {

    static long printExecutionTime(String name, IPerformer performer) {
        long startTime = System.nanoTime();
        performer.action();
        long endTime = System.nanoTime();
        long diff = endTime - startTime;
        String diffStr = diff + "";
        diffStr = diffStr.substring(0, 4) + "-" + diffStr.substring(4);
        System.out.println(name + " takes " + diffStr + "ns.");
        return diff;
    }
}
