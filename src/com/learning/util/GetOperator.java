package com.learning.util;

import com.learning.interfaces.IListOperator;
import com.learning.pojos.SimplePojo;

import java.util.AbstractList;

public class GetOperator implements IListOperator {

    @Override
    public void operate(AbstractList<SimplePojo> list, int times) {

        for (int i = 0; i < times; i++) {
            DataProvider.fillList(list);
        }

        int res = 0;

        for (int i = 0; i < list.size(); i++) {

            SimplePojo pojo = list.get(i);
            if (pojo != null)
                res = pojo.getIntVal();
        }
    //    System.out.println(res);
    }
}
