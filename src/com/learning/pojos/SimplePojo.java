package com.learning.pojos;

public class SimplePojo {

    private String strVal;
    private int intVal;

    public SimplePojo(String strVal, int intVal) {
        this.strVal = strVal;
        this.intVal = intVal;
    }

    public String getStrVal() {
        return strVal;
    }

    public void setStrVal(String strVal) {
        this.strVal = strVal;
    }

    public int getIntVal() {
        return intVal;
    }

    public void setIntVal(int intVal) {
        this.intVal = intVal;
    }

    @Override
    public String toString() {
        return strVal + "-" + intVal;
    }
}
